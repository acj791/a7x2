package _untouchable_.thingy; 


/**
 * LabExam11113_4XIB1-P1    (PTP-???)<br />
 *<br />
 * Der enum Weighte beschreibt das moegliche Gewicht eines Items.
 *<br />
 * 
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1113_4XIB1-P1_172v01_18mmdd_v00
 */
public enum Weight {
     LIGHT, MEDIUM, HEAVY
}//enum
